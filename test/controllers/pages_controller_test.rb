require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get joe" do
    get :joe
    assert_response :success
  end

  test "should get tim" do
    get :tim
    assert_response :success
  end

end
